package fr.iut.iem.android.flippy.adapter;

import java.util.ArrayList;
import java.util.List;

import fr.iut.iem.android.flippy.R;
import fr.iut.iem.android.flippy.R.layout;
import fr.iut.iem.android.flippy.object.Frame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class FrameAdapter extends BaseAdapter {

	private List<Frame> mFrames;
	private Context mContext;
	
	public FrameAdapter(Context context) {
		this(context, new ArrayList<Frame>());
	}
	
	public FrameAdapter(Context context, List<Frame> frames) {
		mContext = context;
		mFrames = frames;
	}
	
	@Override
	public int getCount() {
		return mFrames.size();
	}

	@Override
	public Object getItem(int position) {
		return mFrames.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			convertView = LayoutInflater.from(mContext).inflate(R.layout.frame_line_item, parent, false);
		}
		
		ImageView img = (ImageView) convertView.findViewById(R.id.frameImg);

		Frame frame = mFrames.get(position);
		if(frame.getmBitmap() != null && frame.getmBitmap().length > 0){
			Bitmap bitmap = BitmapFactory.decodeByteArray(frame.getmBitmap(), 0, frame.getmBitmap().length);//Bitmap.createScaledBitmap(, dpToPx(400), dpToPx(150), false);
			img.setImageBitmap(bitmap);
		}else{
			img.setImageBitmap(null);
		}
				
		return convertView;
	}
	
	public int dpToPx(int dp) {
	    DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
	    return px;
	}

}
