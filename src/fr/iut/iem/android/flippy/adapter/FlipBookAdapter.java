package fr.iut.iem.android.flippy.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import fr.iut.iem.android.flippy.R;
import fr.iut.iem.android.flippy.R.id;
import fr.iut.iem.android.flippy.R.layout;
import fr.iut.iem.android.flippy.object.FlipBook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FlipBookAdapter extends BaseAdapter {

	private List<FlipBook> mProjects;
	private Context mContext;
	
	public FlipBookAdapter(Context context){
		this(context, new ArrayList<FlipBook>());
	}
	
	public FlipBookAdapter(Context context, List<FlipBook> projects){
		mProjects = projects;
		mContext = context;
	}
	
	@Override
	public int getCount() {
		return mProjects.size();
	}

	@Override
	public FlipBook getItem(int position) {
		return mProjects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			convertView = LayoutInflater.from(mContext).inflate(R.layout.project_line_item, null);
		}
		
		TextView projectName = (TextView) convertView.findViewById(R.id.project_name);
		
		FlipBook project = getItem(position);
		String name = project.getName();
		projectName.setText(name);
		
		return convertView;
	}

}
