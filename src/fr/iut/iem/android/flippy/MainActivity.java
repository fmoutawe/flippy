package fr.iut.iem.android.flippy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;
import fr.iut.iem.android.flippy.adapter.FlipBookAdapter;
import fr.iut.iem.android.flippy.adapter.FrameAdapter;
import fr.iut.iem.android.flippy.fragment.AnimatedGifDialogFragment;
import fr.iut.iem.android.flippy.fragment.EditFlipBookDialogFragment;
import fr.iut.iem.android.flippy.fragment.EditFlipBookDialogFragment.EditFlipBookCallback;
import fr.iut.iem.android.flippy.object.FlipBook;
import fr.iut.iem.android.flippy.object.Frame;
import fr.iut.iem.android.flippy.other.AnimatedGifEncoder;
import fr.iut.iem.android.flippy.other.UploadCallback;
import fr.iut.iem.android.flippy.other.UploadToServer;
import fr.iut.iem.android.flippy.provider.flipbook.FlipbookColumns;
import fr.iut.iem.android.flippy.provider.flipbook.FlipbookContentValues;
import fr.iut.iem.android.flippy.provider.flipbook.FlipbookCursorWrapper;
import fr.iut.iem.android.flippy.provider.flipbook.FlipbookSelection;
import fr.iut.iem.android.flippy.provider.frame.FrameColumns;
import fr.iut.iem.android.flippy.provider.frame.FrameContentValues;
import fr.iut.iem.android.flippy.provider.frame.FrameCursorWrapper;
import fr.iut.iem.android.flippy.provider.frame.FrameSelection;

public class MainActivity extends FragmentActivity implements
		AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener,
		EditFlipBookCallback, UploadCallback {

	public static final String TAG = MainActivity.class.getSimpleName();
	public static final String FRAME_EDIT = "fr.iut.iem.android.flippy.intent.action.FRAME_EDIT";
	private final String mFolderPath = Environment
			.getExternalStorageDirectory() + "/flippy_gif/";
	private final String mWebPhpPath = "http://www.ivelfan.com/index.php";
	private final String mWebFolderPath = "http://www.ivelfan.com/uploads/";

	// FlipBook instance object
	private FlipBook mCurrentFlipBook;
	private List<FlipBook> mFlipBooks;
	private FlipBookAdapter mFlipBookAdapter;
	private ListView mListViewFlipBook;
	private UploadToServer mUploadToServer;

	// Frame instance object
	private Frame mCurrentFrame;
	private List<Frame> mFrames;
	private FrameAdapter mFrameAdapter;
	private GridView mGridViewFrames;

	private BroadcastReceiver mBroadcastReceiver;
	ProgressDialog mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mBroadcastReceiver = new FrameEditReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(FRAME_EDIT);
		registerReceiver(mBroadcastReceiver, filter);

		// Set the flipbook list view
		mListViewFlipBook = (ListView) findViewById(R.id.projects);

		mFlipBooks = new ArrayList<FlipBook>();
		mFlipBookAdapter = new FlipBookAdapter(this, mFlipBooks);
		mListViewFlipBook.setAdapter(mFlipBookAdapter);
		mListViewFlipBook.setOnItemClickListener(this);
		mListViewFlipBook.setOnItemLongClickListener(this);

		// load FlipBooks into mFlipBooks
		loadFlipBooks();

		// Set the frames grid view
		mGridViewFrames = (GridView) findViewById(R.id.frames);

		mFrames = new ArrayList<Frame>();
		mFrameAdapter = new FrameAdapter(this, mFrames);
		mGridViewFrames.setAdapter(mFrameAdapter);
		mGridViewFrames.setOnItemClickListener(this);
		mGridViewFrames.setOnItemLongClickListener(this);

		if (mCurrentFlipBook != null) {
			loadFrame(mCurrentFlipBook.getId());

			mListViewFlipBook.setItemChecked(
					mFlipBooks.indexOf(mCurrentFlipBook), true);
			if (mCurrentFrame != null)
				mGridViewFrames.setItemChecked(mFrames.indexOf(mCurrentFrame),
						true);
		}

		mUploadToServer = new UploadToServer(mWebPhpPath, mFolderPath);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (mCurrentFlipBook != null) {
			switch (item.getItemId()) {
			case R.id.action_play:
				play();
				break;
			case R.id.action_edit:
				edit(mCurrentFlipBook);
				break;
			case R.id.action_remove:
				DeleteFlipbookDialog popup = new DeleteFlipbookDialog();
				popup.show(getSupportFragmentManager(), "Confirmer suppression");
				break;
			case R.id.action_share:
				share();
				break;
			default:
				return false;
			}
		} else
			Toast.makeText(this, R.string.no_flipbook_message,
					Toast.LENGTH_LONG).show();

		return true;
	}

	public void onClick(View view) {
		if (view.getId() == R.id.new_project)
			addProject();
		else if (mCurrentFlipBook != null) {
			switch (view.getId()) {
			case R.id.new_action:
				Frame frame = addNewFrameInCurrentFlipbook();
				saveFrameInCurrentFlipbook(frame);
				mFrameAdapter.notifyDataSetChanged();
				editFrame();
				break;
			case R.id.previous:
				if (mCurrentFrame != null) {
					switchCurrentWithPreviousFrame();
					mFrameAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(this, R.string.no_frame_message,
							Toast.LENGTH_LONG).show();
				}
				break;
			case R.id.full_screen:
				if (mCurrentFrame != null) {
					editFrame();
				} else {
					Toast.makeText(this, R.string.no_frame_message,
							Toast.LENGTH_LONG).show();
				}
				break;
			case R.id.next:
				if (mCurrentFrame != null) {
					switchCurrentWithNextFrame();
					mFrameAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(this, R.string.no_frame_message,
							Toast.LENGTH_LONG).show();
				}
				break;
			case R.id.remove:
				if (mCurrentFrame != null) {
					deleteCurrentFrameFromFlipbook();
				} else {
					Toast.makeText(this, R.string.no_frame_message,
							Toast.LENGTH_LONG).show();
				}
				break;
			default:
				return;
			}
		} else
			Toast.makeText(this, R.string.no_flipbook_message,
					Toast.LENGTH_LONG).show();
	}

	private void deleteCurrentFrameFromFlipbook() {
		if (mCurrentFrame != null) {
			int currentFrameIndex = mFrames.indexOf(mCurrentFrame);

			FrameSelection whereFrame = new FrameSelection();
			whereFrame.id(mCurrentFrame.getId());
			if (getContentResolver().delete(FrameColumns.CONTENT_URI,
					whereFrame.sel(), whereFrame.args()) > 0) {
				mFrames.remove(mCurrentFrame);

				// Get the previous frame, or next if there's no previous. Set
				// to
				// null if no more frame exists.
				try {
					mCurrentFrame = mFrames.get(currentFrameIndex - 1);
				} catch (IndexOutOfBoundsException e1) {
					try {
						mCurrentFrame = mFrames.get(currentFrameIndex);
					} catch (IndexOutOfBoundsException e2) {
						mCurrentFrame = null;
					}
				}

				mGridViewFrames.setItemChecked(mFrames.indexOf(mCurrentFrame),
						true);
				mFrameAdapter.notifyDataSetChanged();
			}
		}
	}

	public void addProject() {
		EditFlipBookDialogFragment editFlipBookDialogFragment = EditFlipBookDialogFragment.Builder
				.create(null, this);
		editFlipBookDialogFragment.setStyle(DialogFragment.STYLE_NORMAL,
				R.style.RedTheme);
		editFlipBookDialogFragment.show(getSupportFragmentManager(), "dialog");
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long arg3) {
		Object obj = adapter.getItemAtPosition(position);
		if (obj instanceof FlipBook) {
			if (mCurrentFlipBook != null && mCurrentFlipBook.equals(obj))
				edit(mCurrentFlipBook);
			else {
				mCurrentFlipBook = (FlipBook) obj;

				loadFrame(mCurrentFlipBook.getId());
				mFrameAdapter.notifyDataSetChanged();
			}
		}
		if (obj instanceof Frame) {
			if (mCurrentFrame != null && mCurrentFrame.equals(obj))
				editFrame();
			else
				mCurrentFrame = (Frame) obj;
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapter, View view,
			int position, long arg3) {
		Object obj = adapter.getItemAtPosition(position);
		onItemClick(adapter, view, position, arg3);
		if (obj instanceof FlipBook) {
			FlipBook flipBook = (FlipBook) adapter.getItemAtPosition(position);
			edit(flipBook);
			return true;
		}
		if (obj instanceof Frame) {
			editFrame();
		}
		return false;
	}

	public void editFrame() {
		Intent intent = new Intent(this, DrawActivity.class);
		intent.putExtra("frame_id", mCurrentFrame.getId());
		int index = mFrames.indexOf(mCurrentFrame);
		if ((index - 1) >= 0) {
			intent.putExtra("previous_id", mFrames.get(index - 1).getId());
		}
		startActivity(intent);
	}

	public void edit(FlipBook flipBook) {
		EditFlipBookDialogFragment editFlipBookDialogFragment = EditFlipBookDialogFragment.Builder
				.create(flipBook, this);
		editFlipBookDialogFragment.setStyle(DialogFragment.STYLE_NORMAL,
				R.style.RedTheme);
		editFlipBookDialogFragment.show(getSupportFragmentManager(), "tag");
	}

	private Frame addNewFrameInCurrentFlipbook() {
		Frame frame = new Frame(mCurrentFlipBook.getId());
		mFrames.add(frame);
		mCurrentFrame = frame;
		mGridViewFrames.setItemChecked(mFrames.indexOf(mCurrentFrame), true);
		return frame;
	}

	public void play() {
		if (mCurrentFlipBook != null && mFrames.size() > 0) {

			AnimatedGifDialogFragment animatedGifDialogFragment = AnimatedGifDialogFragment.Builder
					.intantiate(mCurrentFlipBook, mFrames);
			animatedGifDialogFragment.setStyle(DialogFragment.STYLE_NORMAL,
					R.style.RedTheme);
			animatedGifDialogFragment.show(getSupportFragmentManager(),
					"animated_dialog");
		}
	}

	public void delete() {
		int flipbookIndex = mFlipBooks.indexOf(mCurrentFlipBook);

		FlipbookSelection where = new FlipbookSelection();
		where.id(mCurrentFlipBook.getId());
		if (getContentResolver().delete(FlipbookColumns.CONTENT_URI,
				where.sel(), where.args()) > 0) {
			mFrames.clear();
			FrameSelection whereFrame = new FrameSelection();
			whereFrame.flipbookId(mCurrentFlipBook.getId());
			getContentResolver().delete(FrameColumns.CONTENT_URI,
					whereFrame.sel(), whereFrame.args());
			mFlipBooks.remove(mCurrentFlipBook);
		}

		// Get the previous flipbook, or next if there's no previous. Set to
		// null if no more flipbook exists.
		try {
			mCurrentFlipBook = mFlipBooks.get(flipbookIndex - 1);
		} catch (IndexOutOfBoundsException e1) {
			try {
				mCurrentFlipBook = mFlipBooks.get(flipbookIndex);
			} catch (IndexOutOfBoundsException e2) {
				mCurrentFlipBook = null;
			}
			
		}
		if (mCurrentFlipBook != null) {
			loadFrame(mCurrentFlipBook.getId());
			mListViewFlipBook.setItemChecked(mFlipBooks.indexOf(mCurrentFlipBook), true);
		} else {
			mFrames.clear();
		}
		mFlipBookAdapter.notifyDataSetChanged();
		mFrameAdapter.notifyDataSetChanged();
	}

	public void share() {

		createGif(mFrames, mCurrentFlipBook.getId() + ".gif", mCurrentFlipBook.getSpeed());
		
		mUploadToServer.upload(this, mCurrentFlipBook.getId() + ".gif", this);
	}

	public void createGif(List<Frame> frames, String gifName, int duration) {
		File fichier = new File(mFolderPath);

		if (!fichier.isDirectory()) {
			fichier.mkdir();
		}

		AnimatedGifEncoder e1 = new AnimatedGifEncoder();
		e1.start(mFolderPath + gifName);
		e1.setDelay(duration);

		for (Frame frame : frames) {
			byte[] bytes = frame.getmBitmap();
			if (bytes != null && bytes.length > 0) {
				e1.addFrame(BitmapFactory.decodeByteArray(bytes, 0,
						bytes.length));
			}
		}

		e1.finish();

	}

	@Override
	public void onFlipBookAdded(FlipBook flipBook) {
		mCurrentFlipBook = flipBook;
		
		mFrames.clear();
		if (flipBook.getId() < 0 && !mFlipBooks.contains(flipBook)) {
			mFlipBooks.add(flipBook);
			FlipbookContentValues values = new FlipbookContentValues();
			values.putName(flipBook.getName()).putSpeed(flipBook.getSpeed());
			Uri insertFlipBook = getContentResolver().insert(
					FlipbookColumns.CONTENT_URI, values.getContentValues());
			int id = (int) ContentUris.parseId(insertFlipBook);
			flipBook.setId(id);

			Frame frame = addNewFrameInCurrentFlipbook();
			saveFrameInCurrentFlipbook(frame);
			editFrame();
		} else {
			FlipbookContentValues values = new FlipbookContentValues();
			values.putName(flipBook.getName()).putSpeed(flipBook.getSpeed());
			FlipbookSelection where = new FlipbookSelection();
			where.id(flipBook.getId());
			getContentResolver().update(FlipbookColumns.CONTENT_URI,
					values.getContentValues(), where.sel(), where.args());
		}
		loadFrame(mCurrentFlipBook.getId());
		
		mListViewFlipBook.setItemChecked(mFlipBooks.indexOf(mCurrentFlipBook), true);

		mFlipBookAdapter.notifyDataSetChanged();
		mFrameAdapter.notifyDataSetChanged();

	}

	public void loadFrame(int flipBookId) {
		mFrames.clear();

		FrameSelection where = new FrameSelection();
		where.flipbookId(flipBookId);
		Cursor c = getContentResolver().query(FrameColumns.CONTENT_URI, null,
				where.sel(), where.args(), null);
		FrameCursorWrapper frameCursorWrapper = null;
		while (c.moveToNext()) {
			frameCursorWrapper = new FrameCursorWrapper(c);
			Frame frame = new Frame((int) frameCursorWrapper.getId(),
					frameCursorWrapper.getFlipbookId(),
					frameCursorWrapper.getBitmap());
			mFrames.add(frame);
		}

		mCurrentFrame = null;
		if (mFrames.size() > 0) {
			mCurrentFrame = mFrames.get(0);
			mGridViewFrames
					.setItemChecked(mFrames.indexOf(mCurrentFrame), true);
			// mFrameAdapter.setCurrentPosition(0);
			// mFrameAdapter.notifyDataSetChanged();
		}
	}

	private void loadFlipBooks() {
		mFlipBooks.clear();

		FlipbookSelection where = new FlipbookSelection();
		Cursor c = getContentResolver().query(FlipbookColumns.CONTENT_URI,
				null, where.sel(), where.args(), null);
		FlipbookCursorWrapper flipbookCursorWrapper = null;
		while (c.moveToNext()) {
			flipbookCursorWrapper = new FlipbookCursorWrapper(c);
			String name = flipbookCursorWrapper.getName();
			Integer id = (int) flipbookCursorWrapper.getId();
			Integer speed = flipbookCursorWrapper.getSpeed();
			FlipBook flipBook = new FlipBook(id, name, speed);
			mFlipBooks.add(flipBook);
		}
		if (flipbookCursorWrapper != null) {
			flipbookCursorWrapper.close();
		}

		mCurrentFlipBook = null;

		if (mFlipBooks.size() > 0) {
			mCurrentFlipBook = mFlipBooks.get(0);
		}

	}

	private void saveFrameInCurrentFlipbook(Frame frameToSave) {
		if (mCurrentFlipBook != null) {
			FrameContentValues values = new FrameContentValues();
			values.putBitmapNull().putFlipbookId(mCurrentFlipBook.getId());
			Uri insertedFrame = getContentResolver().insert(
					FrameColumns.CONTENT_URI, values.getContentValues());
			int id = (int) ContentUris.parseId(insertedFrame);
			frameToSave.setId(id);
		}
	}

	@Override
	public void onUploadEnded(boolean result, String fileName) {
		if (result) {
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("text/plain");
			share.putExtra(Intent.EXTRA_TEXT, mWebFolderPath + fileName);
			startActivity(Intent
					.createChooser(share, "Partager votre FlipBook"));

		} else {
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(MainActivity.this,
							"Impossible d'envoyer le FlipBook",
							Toast.LENGTH_SHORT).show();
				}
			});

		}
	}

	private void switchCurrentWithNextFrame() {
		int currentFrameIndex = mFrames.indexOf(mCurrentFrame);
		if (currentFrameIndex < mFrames.size() - 1) {
			// Next frame DOES exist.
			int nextFrameIndex = currentFrameIndex + 1;
			Frame nextFrame = mFrames.get(nextFrameIndex);
			byte[] bmpTemp = nextFrame.getmBitmap();
			nextFrame.setmBitmap(mCurrentFrame.getmBitmap());
			mCurrentFrame.setmBitmap(bmpTemp);

			saveFrame(mCurrentFrame);
			saveFrame(nextFrame);
			
			mCurrentFrame = nextFrame;
			mGridViewFrames.setItemChecked(mFrames.indexOf(mCurrentFrame), true);
		}
	}

	private void switchCurrentWithPreviousFrame() {
		int currentFrameIndex = mFrames.indexOf(mCurrentFrame);
		if (currentFrameIndex > 0) {
			// Previous frame DOES exist.
			int previousFrameIndex = currentFrameIndex - 1;
			Frame previousFrame = mFrames.get(previousFrameIndex);
			byte[] bmpTemp = previousFrame.getmBitmap();
			previousFrame.setmBitmap(mCurrentFrame.getmBitmap());
			mCurrentFrame.setmBitmap(bmpTemp);

			saveFrame(mCurrentFrame);
			saveFrame(previousFrame);
			
			mCurrentFrame = previousFrame;
			mGridViewFrames.setItemChecked(mFrames.indexOf(mCurrentFrame), true);
		}
	}

	private void saveFrame(Frame _frameToSave) {
		FrameContentValues values = new FrameContentValues();
		values.putBitmap(_frameToSave.getmBitmap());
		FrameSelection where = new FrameSelection();
		where.id(_frameToSave.getId());
		getContentResolver().update(FrameColumns.CONTENT_URI,
				values.getContentValues(), where.sel(), where.args());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mBroadcastReceiver);
	}

	public class FrameEditReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (mCurrentFrame != null) {
				FrameSelection where = new FrameSelection();
				where.id(mCurrentFrame.getId());
				Cursor c = getContentResolver().query(FrameColumns.CONTENT_URI,
						null, where.sel(), where.args(), null);
				while (c.moveToNext()) {
					FrameCursorWrapper frameCursorWrapper = new FrameCursorWrapper(
							c);
					mCurrentFrame.setmBitmap(frameCursorWrapper.getBitmap());
					frameCursorWrapper.close();
				}
				mFrameAdapter.notifyDataSetChanged();
			}
		}
	}

	public static class DeleteFlipbookDialog extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the Builder class for convenient dialog construction
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(R.string.delete_flipbook_message)
					.setPositiveButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dismiss();
								}
							})
					.setNegativeButton(R.string.delete,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									if (getActivity() != null)
										((MainActivity) getActivity()).delete();
								}
							});
			// Create the AlertDialog object and return it
			return builder.create();
		}

		public DeleteFlipbookDialog() {
		}
	}
}
