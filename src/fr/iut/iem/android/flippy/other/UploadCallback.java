package fr.iut.iem.android.flippy.other;

public interface UploadCallback {
	void onUploadEnded(boolean result, String fileName);
}
