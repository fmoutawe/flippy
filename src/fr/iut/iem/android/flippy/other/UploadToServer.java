package fr.iut.iem.android.flippy.other;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class UploadToServer {

	ProgressDialog mDialog;
	String mServerUrl;
	String mFolderPath;
	String mFileName;
	boolean mResultUpload;

	public UploadToServer(String serverUrl, String folderPath) {
		mServerUrl = serverUrl;
		mFolderPath = folderPath;
		mResultUpload = false;
	}

	public void upload(final Context context, String fileName,
			final UploadCallback uploadCallback) {
		mFileName = fileName;

		File sourceFile = new File(mFolderPath + mFileName);

		if (haveNetworkConnection(context) && sourceFile.isFile()) {

			mDialog = ProgressDialog.show(context, "", "Envoi du FlipBook...",
					true);

			new Thread(new Runnable() {
				public void run() {
					mResultUpload = uploadFile();

					uploadCallback.onUploadEnded(mResultUpload, mFileName);

					mResultUpload = false;
				}
			}).start();

		} else {
			uploadCallback.onUploadEnded(mResultUpload, mFileName);
		}
	}

	private boolean uploadFile() {

		String sourceFilePath = mFolderPath + mFileName;
		System.out.println(sourceFilePath);
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;

		int serverResponseCode;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		try {
			FileInputStream fileInputStream = new FileInputStream(new File(
					sourceFilePath));

			URL url = new URL(mServerUrl);
			connection = (HttpURLConnection) url.openConnection();

			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream
					.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
							+ sourceFilePath + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens
					+ lineEnd);

			serverResponseCode = connection.getResponseCode();

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();

			if (serverResponseCode == 200) {

				Log.d("Upload file to server (Ok)", "Reussi");

				mDialog.dismiss();

				return true;

			}
		} catch (Exception ex) {
			Log.d("Upload file to server (Exception)", ex.getMessage());
		}

		Log.d("Upload file to server (Error)", "Erreur");

		mDialog.dismiss();

		return false;
	}

	private boolean haveNetworkConnection(Context context) {

		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();

		for (NetworkInfo ni : netInfo) {

			if (ni.getTypeName().equalsIgnoreCase("WIFI")) {

				if (ni.isConnected()) {

					haveConnectedWifi = true;

				}
			}

			if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {

				if (ni.isConnected()) {

					haveConnectedMobile = true;

				}

			}

		}

		return haveConnectedWifi || haveConnectedMobile;

	}
}
