package fr.iut.iem.android.flippy.provider.frame;

import android.net.Uri;
import android.provider.BaseColumns;

import fr.iut.iem.android.flippy.provider.DataProvider;

/**
 * Columns for the {@code frame} table.
 */
public interface FrameColumns extends BaseColumns {
    String TABLE_NAME = "frame";
    Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    String _ID = BaseColumns._ID;
    String FLIPBOOK_ID = "flipbook_id";
    String BITMAP = "bitmap";
    Integer BITMAP_COL = 2;

    String DEFAULT_ORDER = _ID;

	// @formatter:off
    String[] FULL_PROJECTION = new String[] {
            _ID,
            FLIPBOOK_ID,
            BITMAP
    };
    // @formatter:on
}