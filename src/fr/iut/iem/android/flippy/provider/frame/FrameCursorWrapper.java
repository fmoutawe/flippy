package fr.iut.iem.android.flippy.provider.frame;

import java.util.Date;

import android.database.Cursor;

import fr.iut.iem.android.flippy.provider.base.AbstractCursorWrapper;

/**
 * Cursor wrapper for the {@code frame} table.
 */
public class FrameCursorWrapper extends AbstractCursorWrapper {
    public FrameCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code flipbook_id} value.
     * Can be {@code null}.
     */
    public Integer getFlipbookId() {
        return getIntegerOrNull(FrameColumns.FLIPBOOK_ID);
    }

    /**
     * Get the {@code bitmap} value.
     * Can be {@code null}.
     */
    public byte[] getBitmap() {
    	return getBlob(FrameColumns.BITMAP_COL);
    }
}
