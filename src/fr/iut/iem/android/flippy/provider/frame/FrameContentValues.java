package fr.iut.iem.android.flippy.provider.frame;

import java.util.Date;

import fr.iut.iem.android.flippy.provider.base.AbstractContentValuesWrapper;

/**
 * Content values wrapper for the {@code frame} table.
 */
public class FrameContentValues extends AbstractContentValuesWrapper {

    public FrameContentValues putFlipbookId(Integer value) {
        mContentValues.put(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }

    public FrameContentValues putFlipbookIdNull() {
        mContentValues.putNull(FrameColumns.FLIPBOOK_ID);
        return this;
    }


    public FrameContentValues putBitmap(byte[] value) {
        mContentValues.put(FrameColumns.BITMAP, value);
        return this;
    }

    public FrameContentValues putBitmapNull() {
        mContentValues.putNull(FrameColumns.BITMAP);
        return this;
    }

}
