package fr.iut.iem.android.flippy.provider.frame;

import java.util.Date;

import fr.iut.iem.android.flippy.provider.base.AbstractSelection;

/**
 * Selection for the {@code frame} table.
 */
public class FrameSelection extends AbstractSelection<FrameSelection> {
    public FrameSelection id(long... value) {
        addEquals(FrameColumns._ID, toObjectArray(value));
        return this;
    }

    public FrameSelection flipbookId(Integer... value) {
        addEquals(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }
    
    public FrameSelection flipbookIdNot(Integer... value) {
        addNotEquals(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }

    public FrameSelection flipbookIdGt(int value) {
        addGreaterThan(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }

    public FrameSelection flipbookIdGtEq(int value) {
        addGreaterThanOrEquals(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }

    public FrameSelection flipbookIdLt(int value) {
        addLessThan(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }

    public FrameSelection flipbookIdLtEq(int value) {
        addLessThanOrEquals(FrameColumns.FLIPBOOK_ID, value);
        return this;
    }

    public FrameSelection bitmap(byte[]... value) {
        addEquals(FrameColumns.BITMAP, value);
        return this;
    }
    
    public FrameSelection bitmapNot(byte[]... value) {
        addNotEquals(FrameColumns.BITMAP, value);
        return this;
    }

}
