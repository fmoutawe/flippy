package fr.iut.iem.android.flippy.provider;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import fr.iut.iem.android.flippy.BuildConfig;
import fr.iut.iem.android.flippy.provider.flipbook.FlipbookColumns;
import fr.iut.iem.android.flippy.provider.frame.FrameColumns;

public class DataSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = DataSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_NAME = "flippy.db";
    private static final int DATABASE_VERSION = 1;

    // @formatter:off
    private static final String SQL_CREATE_TABLE_FLIPBOOK = "CREATE TABLE IF NOT EXISTS "
            + FlipbookColumns.TABLE_NAME + " ( "
            + FlipbookColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FlipbookColumns.NAME + " TEXT, "
            + FlipbookColumns.SPEED + " INTEGER "
            + " );";

    private static final String SQL_CREATE_INDEX_FLIPBOOK_NAME = "CREATE INDEX IDX_FLIPBOOK_NAME "
            + " ON " + FlipbookColumns.TABLE_NAME + " ( " + FlipbookColumns.NAME + " );";
    private static final String SQL_CREATE_TABLE_FRAME = "CREATE TABLE IF NOT EXISTS "
            + FrameColumns.TABLE_NAME + " ( "
            + FrameColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FrameColumns.FLIPBOOK_ID + " INTEGER, "
            + FrameColumns.BITMAP + " BLOB "
            + ", CONSTRAINT FK_COMPANY FOREIGN KEY (FLIPBOOK_ID) REFERENCES FLIPBOOK (_ID) ON DELETE CASCADE"
            + " );";

    // @formatter:on

    public DataSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public DataSQLiteOpenHelper(Context context, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        db.execSQL(SQL_CREATE_TABLE_FLIPBOOK);
        db.execSQL(SQL_CREATE_INDEX_FLIPBOOK_NAME);
        db.execSQL(SQL_CREATE_TABLE_FRAME);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
    }
}
