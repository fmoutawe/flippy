package fr.iut.iem.android.flippy.provider.flipbook;

import android.net.Uri;
import android.provider.BaseColumns;

import fr.iut.iem.android.flippy.provider.DataProvider;

/**
 * Columns for the {@code flipbook} table.
 */
public interface FlipbookColumns extends BaseColumns {
    String TABLE_NAME = "flipbook";
    Uri CONTENT_URI = Uri.parse(DataProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    String _ID = BaseColumns._ID;
    String NAME = "name";
    String SPEED = "speed";

    String DEFAULT_ORDER = _ID;

	// @formatter:off
    String[] FULL_PROJECTION = new String[] {
            _ID,
            NAME,
            SPEED
    };
    // @formatter:on
}