package fr.iut.iem.android.flippy.provider.flipbook;

import java.util.Date;

import android.database.Cursor;

import fr.iut.iem.android.flippy.provider.base.AbstractCursorWrapper;

/**
 * Cursor wrapper for the {@code flipbook} table.
 */
public class FlipbookCursorWrapper extends AbstractCursorWrapper {
    public FlipbookCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code name} value.
     * Can be {@code null}.
     */
    public String getName() {
        Integer index = getCachedColumnIndexOrThrow(FlipbookColumns.NAME);
        return getString(index);
    }

    /**
     * Get the {@code speed} value.
     * Can be {@code null}.
     */
    public Integer getSpeed() {
        return getIntegerOrNull(FlipbookColumns.SPEED);
    }
}
