package fr.iut.iem.android.flippy.provider.flipbook;

import java.util.Date;

import fr.iut.iem.android.flippy.provider.base.AbstractContentValuesWrapper;

/**
 * Content values wrapper for the {@code flipbook} table.
 */
public class FlipbookContentValues extends AbstractContentValuesWrapper {

    public FlipbookContentValues putName(String value) {
        mContentValues.put(FlipbookColumns.NAME, value);
        return this;
    }

    public FlipbookContentValues putNameNull() {
        mContentValues.putNull(FlipbookColumns.NAME);
        return this;
    }


    public FlipbookContentValues putSpeed(Integer value) {
        mContentValues.put(FlipbookColumns.SPEED, value);
        return this;
    }

    public FlipbookContentValues putSpeedNull() {
        mContentValues.putNull(FlipbookColumns.SPEED);
        return this;
    }

}
