package fr.iut.iem.android.flippy.provider.flipbook;

import java.util.Date;

import fr.iut.iem.android.flippy.provider.base.AbstractSelection;

/**
 * Selection for the {@code flipbook} table.
 */
public class FlipbookSelection extends AbstractSelection<FlipbookSelection> {
    public FlipbookSelection id(long... value) {
        addEquals(FlipbookColumns._ID, toObjectArray(value));
        return this;
    }

    public FlipbookSelection name(String... value) {
        addEquals(FlipbookColumns.NAME, value);
        return this;
    }
    
    public FlipbookSelection nameNot(String... value) {
        addNotEquals(FlipbookColumns.NAME, value);
        return this;
    }


    public FlipbookSelection speed(Integer... value) {
        addEquals(FlipbookColumns.SPEED, value);
        return this;
    }
    
    public FlipbookSelection speedNot(Integer... value) {
        addNotEquals(FlipbookColumns.SPEED, value);
        return this;
    }

    public FlipbookSelection speedGt(int value) {
        addGreaterThan(FlipbookColumns.SPEED, value);
        return this;
    }

    public FlipbookSelection speedGtEq(int value) {
        addGreaterThanOrEquals(FlipbookColumns.SPEED, value);
        return this;
    }

    public FlipbookSelection speedLt(int value) {
        addLessThan(FlipbookColumns.SPEED, value);
        return this;
    }

    public FlipbookSelection speedLtEq(int value) {
        addLessThanOrEquals(FlipbookColumns.SPEED, value);
        return this;
    }
}
