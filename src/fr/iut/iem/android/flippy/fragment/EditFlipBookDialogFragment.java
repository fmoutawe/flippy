package fr.iut.iem.android.flippy.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import fr.iut.iem.android.flippy.R;
import fr.iut.iem.android.flippy.object.FlipBook;

public class EditFlipBookDialogFragment extends DialogFragment implements
		SeekBar.OnSeekBarChangeListener {

	private EditFlipBookCallback mEditFlipBookCallback;
	private FlipBook mFlipBook;
	private TextView mTextViewSpeed;
	private SeekBar mSeekBarSpeed;
	private EditText mEditTextFlipBookName;

	public static class Builder {
		public static EditFlipBookDialogFragment create(FlipBook flipBook,
				EditFlipBookCallback editFlipBookCallback) {
			EditFlipBookDialogFragment editFlipBook = new EditFlipBookDialogFragment();
			editFlipBook.setFlipBook(flipBook);
			editFlipBook.setEditFlipBookCallback(editFlipBookCallback);
			return editFlipBook;
		}
	}

	public void setFlipBook(FlipBook flipBook) {
		mFlipBook = flipBook;
	}

	public void setEditFlipBookCallback(
			EditFlipBookCallback editFlipBookCallback) {
		mEditFlipBookCallback = editFlipBookCallback;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.edit_project, container, false);
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		// Button
		Button cancel = (Button) view.findViewById(R.id.cancel);
		Button add = (Button) view.findViewById(R.id.add);

		// If flipbook exist, set flipbook values
		if (mFlipBook == null) {
			mFlipBook = new FlipBook(getString(R.string.new_project));
			add.setText("Ajouter");
		} else {
			add.setText("Modifier");
		}

		String name = mFlipBook.getName();
		int speed = mFlipBook.getSpeed();

		// Set dialog title
		getDialog().setTitle(name);

		// Set flipbook name
		mEditTextFlipBookName = (EditText) view
				.findViewById(R.id.flipbook_name);
		mEditTextFlipBookName.setText(name);

		// Set flipbook speed
		mSeekBarSpeed = (SeekBar) view.findViewById(R.id.flipbook_speed);
		mSeekBarSpeed.setProgress(speed - 1);
		mTextViewSpeed = (TextView) view.findViewById(R.id.flipbook_speed_text);
		mTextViewSpeed.setText("x" + speed);

		mSeekBarSpeed.setOnSeekBarChangeListener(this);

		// Button callback
		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v)
			{
				boolean success = false;
				String name = mEditTextFlipBookName.getText().toString();
				if (name != null) {
					mFlipBook.setName(name);
					success = true;
				}
				mFlipBook.setSpeed(mSeekBarSpeed.getProgress() + 1);
				if (success) {
					mEditFlipBookCallback.onFlipBookAdded(mFlipBook);
					dismiss();
				}
			}
		});

		return view;
	}

	public static interface EditFlipBookCallback {
		void onFlipBookAdded(FlipBook flipBook);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		mTextViewSpeed.setText("x" + (progress + 1));

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}
}
