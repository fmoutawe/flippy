package fr.iut.iem.android.flippy.fragment;

import java.util.List;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import fr.iut.iem.android.flippy.R;
import fr.iut.iem.android.flippy.object.FlipBook;
import fr.iut.iem.android.flippy.object.Frame;

public class AnimatedGifDialogFragment extends DialogFragment {

	public static class Builder{
		public static AnimatedGifDialogFragment intantiate(FlipBook currentFlipbook, List<Frame> frames){
			AnimatedGifDialogFragment animatedGifDialogFragment = new AnimatedGifDialogFragment();
			animatedGifDialogFragment.setmCurrentFlipBook(currentFlipbook);
			animatedGifDialogFragment.setmFrames(frames);
			return animatedGifDialogFragment;
		}
	}

	private List<Frame> mFrames;
	private FlipBook mCurrentFlipBook;
	
	private ImageView mPlay;

	private int mPlayCpt;
	private int mDurationPlay;

	private Handler handlerPlay = new Handler();
	private Runnable runnablePlay = new Runnable() {
		@Override
		public void run() {
			byte[] bytes = mFrames.get(mPlayCpt).getmBitmap();

			if (bytes != null && bytes.length > 0) {
				mPlay.setImageBitmap(BitmapFactory.decodeByteArray(
						bytes, 0, bytes.length));
			}

			if ((mFrames.size() - 1) == mPlayCpt) {
				mPlayCpt = 0;
			} else {
				mPlayCpt++;
			}

			handlerPlay.postDelayed(this, mDurationPlay);
		}
	};
	
	public AnimatedGifDialogFragment() {
	}
	
	public List<Frame> getmFrames() {
		return mFrames;
	}
	
	public void setmFrames(List<Frame> mFrames) {
		this.mFrames = mFrames;
	}

	public FlipBook getmCurrentFlipBook() {
		return mCurrentFlipBook;
	}

	public void setmCurrentFlipBook(FlipBook mCurrentFlipBook) {
		this.mCurrentFlipBook = mCurrentFlipBook;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.animated_gif, container, false);
		mPlay = (ImageView) view.findViewById(R.id.play);
		
		mDurationPlay = 300 / mCurrentFlipBook.getSpeed();
		mPlayCpt = 0;
		handlerPlay.postDelayed(runnablePlay, mDurationPlay);
		
		return view;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		handlerPlay.removeCallbacks(runnablePlay);
	}
}
