package fr.iut.iem.android.flippy.object;

import android.graphics.Bitmap;

public class Frame {

	private Integer mProjectId;
	private Integer mId;
	private byte[] mBitmap;

	public Frame(Integer flipBookId) {
		this(-1, flipBookId);
	}

	public Frame(Integer flipBookId, byte[] bitmap) {
		this(-1, flipBookId, bitmap);
	}

	public Frame(Integer id, Integer flipBookId) {
		this(id, flipBookId, null);
	}

	public Frame(Integer id, Integer flipBookId, byte[] bitmap) {
		mProjectId = flipBookId;
		mId = id;
		mBitmap = bitmap;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getId() {
		return mId;
	}

	public void setId(Integer mId) {
		this.mId = mId;
	}

	public byte[] getmBitmap() {
		return mBitmap;
	}

	public void setmBitmap(byte[] mBitmap) {
		this.mBitmap = mBitmap;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Frame){
			return getId().equals(((Frame)o).getId());
		}
		return false;
	}

}
