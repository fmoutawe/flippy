package fr.iut.iem.android.flippy.object;

import android.net.Uri;

public class FlipBook {
	
	private Integer mId;
	private String mName;
	private Integer mSpeed;
	public Uri uri;
	
	public FlipBook(String name){
		this(-1, name);
	}
	
	public FlipBook(Integer id, String name){
		this(id, name, 1);
	}
	
	public FlipBook(Integer id, String name, Integer speed) {
		mId = id;
		mName = name;
		mSpeed = speed;
	}
	
	public String getName(){
		return mName;
	}
	
	public void setName(String name){
		mName = name;
	}
	
	public Integer getSpeed(){
		return mSpeed;
	}
	
	public void setSpeed(Integer speed){
		mSpeed = speed;
	}
	
	public Integer getId(){
		return mId;
	}
	
	public void setId(int id){
		mId = id;
	}
	
	public void save(){
		
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof FlipBook){
			FlipBook flipBook = (FlipBook) o;
			return getId() == flipBook.getId();
		}
		return false;
	}

}
