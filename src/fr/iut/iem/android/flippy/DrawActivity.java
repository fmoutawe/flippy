package fr.iut.iem.android.flippy;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import fr.iut.iem.android.flippy.object.Frame;
import fr.iut.iem.android.flippy.provider.frame.FrameColumns;
import fr.iut.iem.android.flippy.provider.frame.FrameContentValues;
import fr.iut.iem.android.flippy.provider.frame.FrameCursorWrapper;
import fr.iut.iem.android.flippy.provider.frame.FrameSelection;
import fr.iut.iem.android.flippy.view.DrawView;

public class DrawActivity extends FragmentActivity implements View.OnClickListener{

	public static final String TAG = DrawActivity.class.getSimpleName();
	private DrawView drawView;
	private Dialog mDialogColors;
	private ImageButton mButtonColors;
	private ImageButton mButtonPen;
	private ImageButton mButtonBrush;
	private ImageButton mButtonPaintRoll;
	private ImageButton mButtonEraser;
	private Frame mFrame;
	
	private boolean isModified = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		int id = intent.getIntExtra("frame_id", 0);
		if(id <= 0){
			finish();
		}
		
		FrameSelection where = new FrameSelection();
		where.id(id);
		Cursor c = getContentResolver().query(FrameColumns.CONTENT_URI, null, where.sel(), where.args(), null);
		mFrame = null;
		while(c.moveToNext()){
			FrameCursorWrapper frameCursorWrapper = new FrameCursorWrapper(c);
			mFrame = new Frame((int) frameCursorWrapper.getId(), frameCursorWrapper.getFlipbookId(), frameCursorWrapper.getBitmap());
			frameCursorWrapper.close();
		}
		
		if(mFrame == null){
			finish();
		}
		
		setContentView(R.layout.activity_draw);
		FrameLayout drawContainer = (FrameLayout) findViewById(R.id.drawContainer);
		
		if(mFrame.getmBitmap() != null && mFrame.getmBitmap().length > 0){
			Bitmap bitmap = BitmapFactory.decodeByteArray(mFrame.getmBitmap(), 0, mFrame.getmBitmap().length);
			drawView = new DrawView(this, bitmap);
		}else{
			drawView = new DrawView(this, null);
		}
		drawContainer.addView(drawView);
		
		mButtonColors = (ImageButton) findViewById(R.id.colors);
		mButtonColors.setBackgroundColor(drawView.getColor());
		
		mButtonPen = (ImageButton) findViewById(R.id.pen);
		mButtonPen.setBackgroundColor(getResources().getColor(R.color.light_red));		
		mButtonBrush = (ImageButton) findViewById(R.id.brush);
		mButtonPaintRoll = (ImageButton) findViewById(R.id.paint_roll);
		mButtonEraser = (ImageButton) findViewById(R.id.eraser);
		
		ImageView previousImage = (ImageView) findViewById(R.id.previousImage);
		int previousId = intent.getIntExtra("previous_id", 0);
		if(previousId > 0){
			where = new FrameSelection();
			where.id(previousId);
			c = getContentResolver().query(FrameColumns.CONTENT_URI, null, where.sel(), where.args(), null);
			while(c.moveToNext()){
				FrameCursorWrapper frameCursorWrapper = new FrameCursorWrapper(c);
				Frame frame = new Frame((int) frameCursorWrapper.getId(), frameCursorWrapper.getFlipbookId(), frameCursorWrapper.getBitmap());
				if(frame.getmBitmap() != null && frame.getmBitmap().length > 0){
					Bitmap bitmap = BitmapFactory.decodeByteArray(frame.getmBitmap(), 0, frame.getmBitmap().length);
					previousImage.setImageBitmap(bitmap);
				}
				frameCursorWrapper.close();
			}
		}else{
			previousImage.setImageBitmap(null);
		}
	}
	
	public void homeDraw(View v)
	{
		if(isModified)
		{
			UnsavedFrameDialog dialog = new UnsavedFrameDialog();
			dialog.show(getSupportFragmentManager(), "Quit without saving");
		}
		else
			finish();
	}
	
	public void cleanDraw(View v){
		setModified(true);
		drawView.clean();
	}
	
	public void eraserDraw(View v){
		drawView.setErase(true);
		mButtonPen.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		mButtonBrush.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		mButtonPaintRoll.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		mButtonEraser.setBackgroundColor(getResources().getColor(R.color.light_red));
	}
	
	public void penDraw(View v){
		drawView.setErase(false);
		mButtonPen.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		mButtonBrush.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		mButtonPaintRoll.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		mButtonEraser.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_flipbook));
		v.setBackgroundColor(getResources().getColor(R.color.light_red));
		
		switch(v.getId()){
		case R.id.paint_roll:
			drawView.setBrushSize(45);
			break;
		case R.id.brush:
			drawView.setBrushSize(30);
			break;
		case R.id.pen:
		default:
			drawView.setBrushSize(15);
		}
	}
	
	public void undoDraw(View v){
		setModified(true);
		drawView.undo();
	}
	
	public void redoDraw(View v){
		setModified(true);
		drawView.redo();
	}
	
	public void colorsDraw(View v){
		mDialogColors = new Dialog(new ContextThemeWrapper(this, R.style.NoActionBarThemeDialog));
		mDialogColors.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialogColors.setContentView(R.layout.colors_chooser);
		mDialogColors.setCancelable(true);
		ImageButton black = (ImageButton) mDialogColors.findViewById(R.id.black);
		ImageButton red = (ImageButton) mDialogColors.findViewById(R.id.red);
		ImageButton blue = (ImageButton) mDialogColors.findViewById(R.id.blue);
		ImageButton yellow = (ImageButton) mDialogColors.findViewById(R.id.yellow);
		ImageButton green = (ImageButton) mDialogColors.findViewById(R.id.green);
		ImageButton grey = (ImageButton) mDialogColors.findViewById(R.id.grey);
		
		black.setOnClickListener(this);
		red.setOnClickListener(this);
		blue.setOnClickListener(this);
		yellow.setOnClickListener(this);
		green.setOnClickListener(this);
		grey.setOnClickListener(this);
		
		mDialogColors.show();
	}
	
	public void saveDraw(View v){
		Bitmap bitmap = drawView.getBitmap();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		
		mFrame.setmBitmap(byteArray);
		FrameContentValues values = new FrameContentValues();
		values.putFlipbookId(mFrame.getmProjectId()).putBitmap(mFrame.getmBitmap());
		FrameSelection where = new FrameSelection();
		where.id(mFrame.getId());
		if(getContentResolver().update(FrameColumns.CONTENT_URI, values.getContentValues(), where.sel(), where.args()) > 0){
			Intent intent = new Intent(MainActivity.FRAME_EDIT);
			sendBroadcast(intent);
		}
		setModified(false);
	}
	
	@Override
	public void onClick(View v) {
		String colorString = v.getTag().toString();
		drawView.setColor(colorString);
		mButtonColors.setBackgroundColor(drawView.getColor());
		mDialogColors.dismiss();		
	}
	
	@Override
	public void onBackPressed()
	{
		if(isModified)
		{
			UnsavedFrameDialog dialog = new UnsavedFrameDialog();
			dialog.show(getSupportFragmentManager(), "Quit without saving");
		}
		else
			finish();
	}

	public static class UnsavedFrameDialog extends DialogFragment
	{
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(R.string.not_saved_message)
	               .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) 
	                   {
	                	   if(getActivity() != null)
	                	   {
	                		   ((DrawActivity)getActivity()).saveDraw(null);
	                		   ((DrawActivity)getActivity()).finish();
	                	   }
	                   }
	               })
	               .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id)
	                   {
	                	   if(getActivity() != null)
	                		   ((DrawActivity)getActivity()).finish();
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
	    
	    public UnsavedFrameDialog(){}
	}

	public void setModified(boolean _newState)
	{
		isModified = _newState;
	}
}
