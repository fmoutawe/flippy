package fr.iut.iem.android.flippy.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import fr.iut.iem.android.flippy.DrawActivity;

public class DrawView extends View {

	private Bitmap mBitmap;
	private Bitmap mBitmapOld;
	private Canvas mCanvas;
	private Canvas mCanvasOld;
	private Path mPath;
	private Paint mBitmapPaint;
	private Paint mPaint;
	private int mBackgroundColor = 0xFFFFFFFF;
	private int mPaintColor = 0xFF000000;
	private boolean erase = false;
	private List<Pair<Path, Paint>> mPaths;
	private List<Pair<Path, Paint>> mUndonePaths;
	private static final int UNDO = 25;

	public DrawView(Context context, Bitmap bitmap) {
		super(context);

		mPath = new Path();
		mBitmapPaint = new Paint(Paint.LINEAR_TEXT_FLAG);
		mBitmapPaint.setAlpha(240);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(mPaintColor);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.BEVEL);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(15);

		mPaths = new ArrayList<Pair<Path, Paint>>();
		mUndonePaths = new ArrayList<Pair<Path, Paint>>();

		if (bitmap != null) {
			mBitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
			mBitmapOld = bitmap.copy(Bitmap.Config.RGB_565, true);
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		boolean drawColor = false;
		if (mBitmap == null) {
			mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
			mBitmapOld = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
			drawColor = true;
		}
		mCanvas = new Canvas(mBitmap);
		if (drawColor) {
			mCanvas.drawColor(mBackgroundColor);
		} else {
			mCanvas.drawBitmap(mBitmap, 0, 0, null);
		}

		mCanvasOld = new Canvas(mBitmapOld);
		if (drawColor) {
			mCanvasOld.drawColor(mBackgroundColor);
		} else {
			mCanvasOld.drawBitmap(mBitmapOld, 0, 0, null);
		}

		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
		canvas.drawPath(mPath, mPaint);
	}

	private float mX, mY;
	private static final float TOUCH_TOLERANCE = 2;

	private void touchStart(float x, float y) {
		mPath.reset();
		mPath.moveTo(x, y);
		mX = x;
		mY = y;
	}

	private void touchMove(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			mX = x;
			mY = y;
		}
	}

	private void touchUp() {
		if (mPaths.size() > (UNDO - 1)) {
			Pair<Path, Paint> pair = mPaths.get(mPaths.size() - UNDO);
			if (pair != null) {
				mCanvasOld.drawBitmap(mBitmapOld, 0, 0, mBitmapPaint);
				mCanvasOld.drawPath(pair.first, pair.second);
				mPaths = mPaths.subList(mPaths.size() - (UNDO - 1),
						mPaths.size());
			}
		}

		mUndonePaths.clear();
		mPath.lineTo(mX, mY);
		mCanvas.drawPath(mPath, mPaint);
		mPaths.add(new Pair<Path, Paint>(new Path(mPath), new Paint(mPaint)));
		mPath.reset();
		
		((DrawActivity)this.getContext()).setModified(true);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touchStart(x, y);
			break;
		case MotionEvent.ACTION_MOVE:
			touchMove(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
			touchUp();
			invalidate();
			break;
		default:
			break;
		}
		return true;
	}

	public void setColor(String color) {
		invalidate();
		mPaintColor = Color.parseColor(color);
		mPaint.setColor(mPaintColor);
	}

	public int getColor() {
		return mPaintColor;
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public void setBrushSize(float newSize) {
		mPaint.setStrokeWidth(newSize);
	}

	public int getBrushSize() {
		return (int) mPaint.getStrokeWidth();
	}

	public void setErase(boolean isErase) {
		erase = isErase;
		if (erase) {
			mPaint.setColor(mBackgroundColor);
//			mPaint.setAlpha(220);
//			mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.));
		} else {
			mPaint.setColor(mPaintColor);
//			mPaint.setAlpha(255);
//			mPaint.setXfermode(null);
		}
	}

	public void clean() {
		mCanvas.drawColor(mBackgroundColor);
		mCanvasOld.drawColor(mBackgroundColor);
		mPaths.clear();
		mUndonePaths.clear();
		invalidate();
	}

	public void undo() {
		if (mPaths.size() > 0 && mUndonePaths.size() < UNDO) {
			mCanvas.drawColor(mBackgroundColor);
			mUndonePaths.add(mPaths.remove(mPaths.size() - 1));
			mCanvas.drawBitmap(mBitmapOld, 0, 0, mBitmapPaint);
			for (Pair<Path, Paint> pair : mPaths) {
				mCanvas.drawPath(pair.first, pair.second);
			}
			invalidate();
		}
	}

	public void redo() {
		if (mUndonePaths.size() > 0 && mPaths.size() < UNDO) {
			mCanvas.drawColor(mBackgroundColor);
			mPaths.add(mUndonePaths.remove(mUndonePaths.size() - 1));
			mCanvas.drawBitmap(mBitmapOld, 0, 0, mBitmapPaint);
			for (Pair<Path, Paint> pair : mPaths) {
				mCanvas.drawPath(pair.first, pair.second);
			}
			invalidate();
		}
	}
}
